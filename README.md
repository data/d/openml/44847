# OpenML dataset: energy_efficiency

https://www.openml.org/d/44847

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

This dataset looked into assessing the heating load and cooling load requirements of buildings (that is, energy efficiency) as a function of building parameters.
Energy analysis is performed using 12 different building shapes simulated in Ecotect. The buildings differ with respect to the glazing area, the glazing area distribution, and the orientation, amongst other parameters. Various settings as functions of the afore-mentioned characteristics are simulated to obtain 768 building shapes (number of observations in the dataset).

**Attribute Description**

All features describe different properties for the building.

1. *relative_compactness*
2. *surface_area*
3. *wall_area*
4. *roof_area*
5. *overall_height*
6. *orientation*
7. *glazing_area*
8. *glazing_area_distribution*
9. *heating_load* - one possible option for target feature
10. *cooling_load* - one possible option for target feature

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44847) of an [OpenML dataset](https://www.openml.org/d/44847). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44847/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44847/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44847/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

